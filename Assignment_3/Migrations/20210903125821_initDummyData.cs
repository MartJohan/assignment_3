﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Assignment_3.Migrations
{
    public partial class initDummyData : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Characters",
                columns: new[] { "CharacterId", "Alias", "Firstname", "Gender", "Lastname", "PhotoURL" },
                values: new object[,]
                {
                    { 1, "Mall Cop", "Paul", "Male", "Blart", "www.Coolestmallcopever.net" },
                    { 2, "Heisenberg", "Walter", "Male", "White", "www.cookCrystal.org" },
                    { 3, "Nikolay koslov", "Raymond", "Male", "Reddington", "wwwboss.net" }
                });

            migrationBuilder.InsertData(
                table: "Franchises",
                columns: new[] { "FranchiseId", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "Makes your dream work", "Dreamworkds" },
                    { 2, "Makes the best movies, you know it's true", "BollyWood" }
                });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "MovieId", "Director", "FranchiseId", "Genres", "PictureURL", "ReleaseYear", "Title", "TrailerURL" },
                values: new object[] { 1, "Micheal Bay", 1, "Comedy', Fantasy, Non-action", "www.freeMovieForEveryOne.ru\\TotallyNotAScam", new DateTime(2001, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Ice age", "www.FreeTrailers.com\\IceAgeRocks" });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "MovieId", "Director", "FranchiseId", "Genres", "PictureURL", "ReleaseYear", "Title", "TrailerURL" },
                values: new object[] { 2, "Steven Somethingberg", 1, "Action, PG18, Disturbing, Horror", "www.somethingfunny\\haha", new DateTime(2020, 4, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), "Ice age 2", "www.Insaneexplosions.gov" });

            migrationBuilder.InsertData(
                table: "Movies",
                columns: new[] { "MovieId", "Director", "FranchiseId", "Genres", "PictureURL", "ReleaseYear", "Title", "TrailerURL" },
                values: new object[] { 3, "I have no idea Tarantino", 2, "Horror", "www.Hereisjohny.no", new DateTime(1968, 2, 2, 0, 0, 0, 0, DateTimeKind.Unspecified), "The shining", "www.shiningbright.gov" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "CharacterId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "CharacterId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Characters",
                keyColumn: "CharacterId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "MovieId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "MovieId",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Movies",
                keyColumn: "MovieId",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Franchises",
                keyColumn: "FranchiseId",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Franchises",
                keyColumn: "FranchiseId",
                keyValue: 2);
        }
    }
}
