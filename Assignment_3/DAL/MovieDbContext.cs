﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assignment_3.Models;
using Microsoft.EntityFrameworkCore;

namespace Assignment_3.DAL
{
    public class MovieDbContext : DbContext
    {
        public MovieDbContext(DbContextOptions<MovieDbContext> options) : base(options)
        {

        }
        public DbSet<Character> Characters { get; set; }

        public DbSet<Movie> Movies { get; set; }

        public DbSet<Franchise> Franchises { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {

        }

        //Define the many to many relationship and seed data to the database
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Franchise>()
                .HasMany(f => f.Movies)
                .WithOne(m => m.Franchise)
                .OnDelete(DeleteBehavior.ClientCascade);

            //Dummy data
            modelBuilder.Entity<Franchise>().HasData(
                 new Franchise
                 {
                     FranchiseId = 1,
                     Name = "Dreamworkds",
                     Description = "Makes your dream work",
                     Movies = new List<Movie>(),
                 });

             modelBuilder.Entity<Franchise>().HasData(
                     new Franchise
                     {
                         FranchiseId = 2,
                         Name = "BollyWood",
                         Description = "Makes the best movies, you know it's true",
                         Movies = new List<Movie>(),
                     }
                 );

             modelBuilder.Entity<Movie>().HasData(
                     new Movie
                     {
                         MovieId = 1,
                         Title = "Ice age",
                         Genres = "Comedy', Fantasy, Non-action",
                         ReleaseYear = new DateTime(2001, 1, 1),
                         Director = "Micheal Bay",
                         PictureURL = "www.freeMovieForEveryOne.ru\\TotallyNotAScam",
                         TrailerURL = "www.FreeTrailers.com\\IceAgeRocks",
                         Characters = new List<Character>(),
                         FranchiseId = 1
                     }
                 );

             modelBuilder.Entity<Movie>().HasData(
                 new Movie
                 {
                     MovieId = 2,
                     Title = "Ice age 2",
                     Genres = "Action, PG18, Disturbing, Horror",
                     ReleaseYear = new DateTime(2020, 4, 2),
                     Director = "Steven Somethingberg",
                     PictureURL = "www.somethingfunny\\haha",
                     TrailerURL = "www.Insaneexplosions.gov",
                     Characters = new List<Character>(),
                     FranchiseId = 1
                 });

             modelBuilder.Entity<Movie>().HasData(
                 new Movie
                 {
                     MovieId = 3,
                     Title = "The shining",
                     Genres = "Horror",
                     ReleaseYear = new DateTime(1968, 2, 2),
                     Director = "I have no idea Tarantino",
                     PictureURL = "www.Hereisjohny.no",
                     TrailerURL = "www.shiningbright.gov",
                     Characters = new List<Character>(),
                     FranchiseId = 2
                 });

             modelBuilder.Entity<Character>().HasData(
                     new Character
                     {
                         CharacterId = 1,
                         Firstname = "Paul",
                         Lastname = "Blart",
                         Alias = "Mall Cop",
                         Gender = "Male",
                         PhotoURL = "www.Coolestmallcopever.net",
                         Movies = new List<Movie>()
                     }
                 );

             modelBuilder.Entity<Character>().HasData(
                 new Character
                 {
                     CharacterId = 2,
                     Firstname = "Walter",
                     Lastname = "White",
                     Alias = "Heisenberg",
                     Gender = "Male",
                     PhotoURL = "www.cookCrystal.org",
                     Movies = new List<Movie>()
                 }
                 );

              modelBuilder.Entity<Character>().HasData(
                  new Character
                  {
                      CharacterId = 3,
                      Firstname = "Raymond",
                      Lastname = "Reddington",
                      Alias = "Nikolay koslov",
                      Gender = "Male",
                      PhotoURL = "wwwboss.net",
                      Movies = new List<Movie>()
                  }
                  );

        }
    }
}
