﻿using Assignment_3.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment_3.DAL
{
    public interface IFranchiseRepository
    {
        Task<IEnumerable<Franchise>> GetFranchises();

        Task<IEnumerable<Movie>> GetMoviesInFranchise(int id);

        Task<List<Character>> GetCharactersInFranchise(int id);

        Task<Franchise> GetFranchise(int id);

        Task PutFranchise(Franchise franchise);

        Task<Franchise> PostFranchise(Franchise franchise);

        Task<bool> UpdateMoviesInFranchise(int franchiseId, params int[] movieId);

        Task DeleteFranchise(int id);

        bool FranchiseExists(int id);
    }
}
