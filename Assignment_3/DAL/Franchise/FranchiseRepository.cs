﻿using Assignment_3.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment_3.DAL
{
    public class FranchiseRepository : IFranchiseRepository
    {
        private readonly MovieDbContext _context;

        public FranchiseRepository(MovieDbContext context)
        {
            _context = context;
        }
        public async Task DeleteFranchise(int id)
        {
            var franchise = await _context.Franchises.FindAsync(id);
            _context.Franchises.Remove(franchise);
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Gets a franchise with the appropriate id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A new Franchise object</returns>
        public async Task<Franchise> GetFranchise(int id)
        {
            return await _context.Franchises.FindAsync(id);
        }
        /// <summary>
        /// Gets all franchies from the database
        /// </summary>
        /// <returns>IEnumerable object of type Movie if succesfull or null if false</returns>
        public async Task<IEnumerable<Franchise>> GetFranchises()
        {
            return await _context.Franchises
                .Include(f => f.Movies).ToListAsync();
        }

        /// <summary>
        /// Gets all movies in a franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns>An IEnumerable of type Movie containg all the different movies in a franchise</returns>
        public async Task<IEnumerable<Movie>> GetMoviesInFranchise(int id)
        {
            Franchise franchise = await _context.Franchises.FindAsync(id);
            IEnumerable<Movie> list = franchise.Movies;
            return list;
        }

        /// <summary>
        /// Gets all the different characters in a selected franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A list of type character containing all characters in the franchise</returns>
        public async Task<List<Character>> GetCharactersInFranchise(int id)
        {
                Franchise franchise = await _context.Franchises.FindAsync(id);
                List<Character> characterList = new List<Character>();

                if (franchise == null) { return null; }

                foreach (Movie movie in franchise.Movies)
                {
                    foreach (Character character in movie.Characters)
                    {
                        characterList.Add(character);
                    }
                }
                return characterList;
        }

        /// <summary>
        /// Adds a new franchise to the database
        /// </summary>
        /// <param name="franchise"></param>
        /// <returns>True if the addition was successfull, false otherwise</returns>
        public async Task<Franchise> PostFranchise(Franchise franchise)
        {
            _context.Franchises.Add(franchise);
            await _context.SaveChangesAsync();
            return franchise;
        }

        /// <summary>
        /// Updates a franchise with 1 or more movie
        /// </summary>
        /// <param name="franchiseId"></param>
        /// <param name="movieId"></param>
        /// <returns>True if the update was successfull, false otherwise</returns>
        public async Task<bool> UpdateMoviesInFranchise(int franchiseId, params int[] movieId)
        {
            try
            {
                Franchise franchise = _context.Franchises.Find(franchiseId);

                foreach (int id in movieId)
                {
                    Movie movie = await _context.Movies.FindAsync(id);
                    franchise.Movies.Add(movie);
                }

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Partialy updates a franchise resource in the database
        /// </summary>
        /// <param name="id"></param>
        /// <param name="franchise"></param>
        /// <returns>True if the update was successfull, false otherwise</returns>
        public async Task PutFranchise(Franchise franchise)
        {
            _context.Entry(franchise).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Checks wheter or not the the franchise exists in the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns>True if it exists, false otherwie</returns>
        public bool FranchiseExists(int id)
        {
            return _context.Franchises.Any(e => e.FranchiseId == id);
        }

       
    }
}
