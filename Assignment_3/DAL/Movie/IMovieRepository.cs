﻿using Assignment_3.Models;
using Assignment_3.Models.DTO.Movie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment_3.DAL
{
    //Interface for interactions between controler and repository.
    public interface IMovieRepository
    {
        Task<IEnumerable<Movie>> GetMovies();

        Task<Movie> GetMovie(int id);

        Task PutMovie(Movie movie);

        Task<bool> UpdateCharactersInMovie(int movieId, params int[] characterId);

        Task<Movie> PostMovie(Movie movie);

        Task<bool> DeleteMovie(int id);

        bool MovieExists(int id);
    }
}
