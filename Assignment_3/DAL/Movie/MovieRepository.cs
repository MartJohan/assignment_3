﻿using Assignment_3.Models;
using Assignment_3.Models.DTO.Movie;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment_3.DAL
{
    public class MovieRepository : IMovieRepository
    {
        private readonly MovieDbContext _context;
        private readonly IMapper _mapper;
        public MovieRepository(MovieDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Deletes a given movie from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns>True if the delete was successfull, or false if it wasn't</returns>
        public async Task<bool> DeleteMovie(int id)
        {
            Movie movie = await _context.Movies.FindAsync(id);
            _context.Remove(movie);

            await _context.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// Gets a movie with the appropriate id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A new Movie object</returns>
        public async Task<Movie> GetMovie(int id)
        {
            Movie movie = await _context.Movies.Include(m => m.Characters).Where(m => m.MovieId == id).FirstAsync();
            return movie;
        }
        
        /// <summary>
        /// Gets all movies from the database
        /// </summary>
        /// <returns>An IEnumerable object of type Movie if successfull or null if false</returns>
        public async Task<IEnumerable<Movie>> GetMovies()
        {
            return await _context.Movies.Include(m => m.Characters).ToListAsync();
        }

        /// <summary>
        /// Gets all movies in a franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns>An IEnumerable of type Movie containg all the different movies in a franchise</returns>
        public async Task<IEnumerable<Movie>> GetMoviesInFranchise(int id)
        {
            List<Movie> list = await _context.Movies.Where(m => m.FranchiseId == id).ToListAsync();
            return list;
        }

        /// <summary>
        /// Adds a new movie to the database
        /// </summary>
        /// <param name="movie"></param>
        /// <returns>True if the operation was successfull, false otherwise</returns>
        public async Task<Movie> PostMovie(Movie movie)
        {
            _context.Movies.Add(movie);
            await _context.SaveChangesAsync();
            return movie;
        }

        /// <summary>
        /// Takes in a movie id and one or multiple character id's and adds all the characters to the movie.
        /// </summary>
        /// <param name="movieId"></param>
        /// <param name="characterId"></param>
        /// <returns>True if the addition was successfull, false otherwise</returns>
        public async Task<bool> UpdateCharactersInMovie(int movieId, params int[] characterId)
        {
            try
            {
                Movie movie = _context.Movies.Find(movieId);

                foreach (int id in characterId)
                {
                    Character character = await _context.Characters.FindAsync(id);
                    movie.Characters.Add(character);
                }

                return true;
            } catch(Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Updates a given movie
        /// </summary>
        /// <param name="id"></param>
        /// <param name="movie"></param>
        /// <returns>True if the update was successfull or false if it failed</returns>
        public async Task PutMovie(Movie movie)
        {
            _context.Entry(movie).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Checks whether or not the given movie exists in the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns>True if the movie exists, false if it doesn't</returns>
        public bool MovieExists(int id)
        {
            return _context.Movies.Any(e => e.MovieId == id);
        }
    }
}
