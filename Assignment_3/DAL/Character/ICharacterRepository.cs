﻿using Assignment_3.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment_3.DAL
{
    public interface ICharacterRepository
    {
        Task<IEnumerable<Character>> GetCharacters();

        Task<Character> GetCharacter(int id);

        Task PutCharacter(Character character);

        Task<bool> UpdateMovieInCharacters(int characterId, params int[] movieId);
       
        Task<Character> PostCharacter(Character character);

        Task<bool> DeleteCharacter(int id);

        bool CharacterExists(int id);

        Task<List<Character>> GetCharactersInFranchise(int id);
    }
}
