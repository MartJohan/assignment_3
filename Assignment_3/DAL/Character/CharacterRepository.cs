﻿using Assignment_3.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment_3.DAL
{
    public class CharacterRepository : ICharacterRepository
    {

        private readonly MovieDbContext _context;

        public CharacterRepository(MovieDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Deletes a given character from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns>True if the delete was successfull, false otherwise</returns>
        public async Task<bool> DeleteCharacter(int id)
        {
            Character character = await _context.Characters.FindAsync(id);
            _context.Remove(character);

            await _context.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// Gets a given character from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns>An object of type Character</returns>
        public async Task<Character> GetCharacter(int id)
        {
            Character character = await _context.Characters.FindAsync(id);
            return character;
           
        }

        /// <summary>
        /// Gets all characters from the database
        /// </summary>
        /// <returns>An IEnumerable object of type character containing all characters</returns>
        public async Task<IEnumerable<Character>> GetCharacters()
        {
            return await _context.Characters.Include(c => c.Movies).ToListAsync();
        }

        /// <summary>
        /// Adds a new character to the database
        /// </summary>
        /// <param name="character"></param>
        /// <returns>True if the operation was successfull, false otherwise</returns>
        public async Task<Character> PostCharacter(Character character)
        {
            _context.Characters.Add(character);
            await _context.SaveChangesAsync();
            return character;
        }
        
        /// <summary>
        /// Takes in a character id and one or multiple movie id's adds all the movies to the charracter
        /// </summary>
        /// <param name="characterId"></param>
        /// <param name="movieId"></param>
        /// <returns>True if the addtion was succesfull, false otherwise</returns>
        public async Task<bool> UpdateMovieInCharacters(int characterId, params int[] movieId)
        {
            try
            {
                Character character = _context.Characters.Find(characterId);

                foreach (int id in movieId)
                {
                    Movie movie = await _context.Movies.FindAsync(id);
                    character.Movies.Add(movie);
                }

                return true;
            }catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// Updates a given character
        /// </summary>
        /// <param name="id"></param>
        /// <param name="character"></param>
        /// <returns>True if the update was successfull, false otherwise</returns>
        public async Task PutCharacter(Character character)
        {
            _context.Entry(character).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }

        /// <summary>
        /// Checks if a character exists in the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns>True if it exists, false otherwise</returns>
        public bool CharacterExists(int id)
        {
            return _context.Characters.Any(e => e.CharacterId == id);
        }
        /// <summary>
        /// Gets all the different characters in a selected franchise
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A list of type character containing all characters in the franchise</returns>
        public async Task<List<Character>> GetCharactersInFranchise(int id)
        {
            try
            {
                Franchise franchise = await _context.Franchises.FindAsync(id);
                List<Character> characterList = new List<Character>();

                if (franchise == null) { return null; }

                foreach (Movie movie in franchise.Movies)
                {
                    foreach (Character character in movie.Characters)
                    {
                        characterList.Add(character);
                    }
                }

                return characterList;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return null;
            }
        }
    }
}
