﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment_3.Models
{
    public class Movie
    {
        public int MovieId { get; set; }
        [Required]
        [MaxLength(255)]
        public string Title { get; set; }

        //One or multiple genres seperated by commas
        [Required]
        [MaxLength(255)]
        public string Genres { get; set; }

        public DateTime ReleaseYear { get; set; }

        [MaxLength(255)]
        public string Director { get; set; }

        public string PictureURL { get; set; }

        public string TrailerURL { get; set; }

        public int FranchiseId { get; set; }

        public Franchise Franchise { get; set; }

        public ICollection<Character> Characters { get; set; }

    }
}
