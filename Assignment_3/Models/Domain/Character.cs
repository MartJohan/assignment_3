﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment_3.Models
{
    public class Character
    {
        public int CharacterId { get; set; }

        [Required]
        [MaxLength(250)]
        public string Firstname { get; set; }

        [Required]
        [MaxLength(250)]
        public string Lastname { get; set; }

        [MaxLength(250)]
        public string Alias { get; set; }

        [MaxLength(40)]
        public string Gender { get; set; }

        //THIS WIL BE NVARCHAR MAX by defualt
        public string PhotoURL { get; set; }

        public ICollection<Movie> Movies { get; set; }
    }
}
