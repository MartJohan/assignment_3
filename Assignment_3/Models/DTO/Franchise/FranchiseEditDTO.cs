﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment_3.Models.DTO.Franchise
{
    public class FranchiseEditDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        //Might be wrong
        public List<int> Movies = new List<int>();
    }
}
