﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment_3.Models.DTO.Character
{
    public class CharacterReadDTO
    {
        public int Id { get; set; }
        public string Firstname { get; set; }

        public string Lastname { get; set; }

        public string Alias { get; set; }

        public string Gender { get; set; }

        public string PhotoURL { get; set; }
        public List<int> Movies { get; set; }
    }
}
