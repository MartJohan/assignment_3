﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment_3.Models.DTO.Movie
{
    public class MovieCreateDTO
    {
        public string Title { get; set; }

        public string Genres { get; set; }

        public DateTime ReleaseYear { get; set; }

        public string Director { get; set; }

        public string PictureURL { get; set; }

        public string TrailerURL { get; set; }

        public int Franchise { get; set; }
        public List<int> Characters { get; set; }

    }
}
