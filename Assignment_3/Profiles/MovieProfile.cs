﻿using AutoMapper;
using Assignment_3.Models;
using Assignment_3.Models.DTO.Movie;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Assignment_3.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {
   
            CreateMap<Movie, MovieReadDTO>()
                .ForMember(mdto => mdto.Franchise, opt => opt
                .MapFrom(m => m.Franchise.FranchiseId))
                .ForMember(mdto => mdto.Characters, opt => opt
                .MapFrom(m => m.Characters.Select(c=> c.CharacterId).ToArray()))
                .ReverseMap();

            CreateMap<Movie, MovieCreateDTO>()
                .ForMember(mdto => mdto.Franchise, opt => opt
                .MapFrom(m => m.Franchise.FranchiseId))
                .ForMember(mdto => mdto.Characters, opt => opt
                .MapFrom(m => m.Characters.Select(c => c.CharacterId).ToArray()))
                .ReverseMap();
            
            CreateMap<Movie, MovieEditDTO>()
                .ForMember(mdto => mdto.Franchise, opt => opt
                .MapFrom(m => m.Franchise.FranchiseId))
                .ForMember(mdto => mdto.Characters, opt => opt
                .MapFrom(m => m.Characters.Select(c => c.CharacterId).ToArray()))
                .ReverseMap();
        }
    }
}
