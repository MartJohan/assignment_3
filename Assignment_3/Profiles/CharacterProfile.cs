﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assignment_3.Models.DTO.Character;
using Assignment_3.Models;


namespace Assignment_3.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterReadDTO>()
                .ForMember(cdto => cdto.Movies, opt => opt
                .MapFrom(c => c.Movies.Select(m=> m.MovieId).ToArray()))
                .ReverseMap();

            CreateMap<Character, CharacterEditDTO>()
                .ForMember(cdto => cdto.Movies, opt => opt
                .MapFrom(c => c.Movies.Select(m => m.MovieId).ToArray()))
                .ReverseMap();

            CreateMap<Character, CharacterCreateDTO>()
                .ForMember(cdto => cdto.Movies, opt => opt
                .MapFrom(c => c.Movies.Select(m => m.MovieId).ToArray()))
                .ReverseMap();

        }
    }
}
