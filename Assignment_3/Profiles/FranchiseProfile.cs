﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Assignment_3.Models;
using Assignment_3.Models.DTO.Franchise;
using AutoMapper;

namespace Assignment_3.Profiles
{
    public class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<Franchise, FranchiseReadDTO>()
                .ForMember(fdto => fdto.Movies, opt => opt
                .MapFrom(f => f.Movies.Select(f => f.MovieId).ToArray())); 
            
            CreateMap<Franchise, FranchiseEditDTO>()
                .ForMember(fdto => fdto.Movies, opt => opt
                .MapFrom(f => f.Movies.Select(f => f.MovieId).ToArray()));
            
            CreateMap<Franchise, FranchiseCreateDTO>()
                .ForMember(fdto => fdto.Movies, opt => opt
                .MapFrom(f => f.Movies.Select(f => f.MovieId).ToArray()));

        }
    }
}
