﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment_3.DAL;
using Assignment_3.Models;
using AutoMapper;
using Assignment_3.Models.DTO.Franchise;

namespace Assignment_3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FranchisesController : ControllerBase
    {
        private readonly IFranchiseRepository _context;
        private readonly IMapper _mapper;

        public FranchisesController(IFranchiseRepository context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Franchises
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseReadDTO>>> GetFranchises()
        {
            return _mapper.Map<List<FranchiseReadDTO>>(await _context.GetFranchises());
        }

        // GET: api/Franchise/5
        [HttpGet("GetMoviesInFranchise/{id}")]
        public async Task<ActionResult<IEnumerable<Movie>>> GetMoviesInFranchise(int id)
        {
            IEnumerable<Movie> movieList = await _context.GetMoviesInFranchise(id);
            if (movieList != null)
            {
                return Ok(movieList);
            }

            return NotFound();
        }

        // GET: api/Franchise/5
        [HttpGet("GetCharactersInFranchise/{id}")]
        public async Task<ActionResult<IEnumerable<Character>>> GetCharactersInFranchise(int id)
        {
            IEnumerable<Character> characterList = await _context.GetCharactersInFranchise(id);
            if(!characterList.Any() || characterList != null)
            {
                return Ok(characterList);
            }

            return NotFound();
        }

        // GET: api/Franchises/5
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            Franchise franchise = await _context.GetFranchise(id);

            if(franchise == null)
            {
                return NotFound();
            }

            return _mapper.Map<FranchiseReadDTO>(franchise);
        }

        // PUT: api/Franchises/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, FranchiseEditDTO dtoFranchise)
        {
            if (id != dtoFranchise.Id)
            {
                return BadRequest();
            }

            if(_context.FranchiseExists(id))
            {
                return NotFound();
            }

            Franchise domainFranchise = _mapper.Map<Franchise>(dtoFranchise);
            await _context.PutFranchise(domainFranchise);

            return NoContent();

           
        }

        // POST: api/Franchises
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise(FranchiseCreateDTO dtoFranchise)
        {
            Franchise domainFranchise = _mapper.Map<Franchise>(dtoFranchise);

            domainFranchise = await _context.PostFranchise(domainFranchise);

            return CreatedAtAction("GetCoach", new { id = domainFranchise.FranchiseId }, 
                _mapper.Map<FranchiseReadDTO>(domainFranchise));
        }

        [HttpPost("{id}")]
        public async Task<IActionResult> UpdateMovieInFranchise(int id, params int[] movieId)
        {
            bool updateFranchise = await _context.UpdateMoviesInFranchise(id, movieId);

            if(updateFranchise)
            {
                return NoContent();
            }

            return BadRequest();
        }

        // DELETE: api/Franchises/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            if(!_context.FranchiseExists(id))
            {
                return NotFound();
            }

            await _context.DeleteFranchise(id);

            return NoContent();
        }

    }
}
