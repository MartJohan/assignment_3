﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Assignment_3.DAL;
using Assignment_3.Models;
using AutoMapper;
using Assignment_3.Models.DTO.Character;

namespace Assignment_3.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CharactersController : ControllerBase
    {
        private readonly ICharacterRepository _context;
        private readonly IMapper _mapper;

        public CharactersController(ICharacterRepository context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/Characters
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
            return Ok(_mapper.Map<List<CharacterReadDTO>>(await _context.GetCharacters()));
        }

        // GET: api/Characters/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            Character character = await _context.GetCharacter(id);

            if (character == null)
            {
                return NotFound();
            }

            return _mapper.Map<CharacterReadDTO>(character);
        }

        // PUT: api/Characters/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, CharacterEditDTO dtoCharacter)
        {
            if (id != dtoCharacter.Id)
            {
                return BadRequest();
            }

            if (!_context.CharacterExists(id))
            {
                return NotFound();
            }

            Character domainCharacter = _mapper.Map<Character>(dtoCharacter);
            await _context.PutCharacter(domainCharacter);

            return NoContent();
        }

        // POST: api/Characters
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult> PostCharacter(CharacterCreateDTO dtoCharacter)
        {
            Character domainCharacter = _mapper.Map<Character>(dtoCharacter);

            domainCharacter = await _context.PostCharacter(domainCharacter);

            return CreatedAtAction("GetMovie", new { id = domainCharacter.CharacterId }, _mapper.Map<CharacterReadDTO>(domainCharacter));
        }


        // POST: api/Characters
        [HttpPost("{id}")]
        public async Task<IActionResult> UpdateMoviesInCharacter(int id, params int[] moviesId)
        {
            bool updateCharacter = await _context.UpdateMovieInCharacters(id, moviesId);

            if (updateCharacter)
            {
                return NoContent();
            }

            return BadRequest();
        }

        // DELETE: api/Characters/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteCharacter(int id)
        {
            bool deleteOk = await _context.DeleteCharacter(id);

            if(deleteOk)
            {
                return Ok();
            }
            return BadRequest();
        }
    }
}
