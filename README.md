# Assignemnt 3

### This assignment was created by the duo, Martin Stenberg and Martin Johansen

The project contains two parts:

### Part 1:

Use Entity Framework Code first to create a database

### Part 2:

Create a Web API in ASP.NET Core


Known issues:
The project contains an error with mapping types, this occurs when a user tries to add a resoruce to the database. Works fine without the use of AutoMapper.
